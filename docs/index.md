
Ce site  __TEST__ ne propose pas encore mes supports de cours et d'exercices,
mais cela viendra un jour, pour l'instant je découvre et je joue !


## Mathématiques
### Cours et exercices de seconde

* [Seconde](Maths_seconde/)

### Cours et exercices de Tronc commun et de Spécialité de Terminale STI2D

* [Terminale_STI2D](Maths_TleSTI2D)


## Informatique
* [Terminale_NSI](NSI_Spécialité_Terminle)



### Informations

Ce site et les exercices qu'il contient sont placés par <a href="https://github.com/UrsulaMR" target="_blank">UrsulaMR</a>
    &copy; 2021 sous licence <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr" target="_blank">CC BY-NC-SA 4.0</a>
    <br> Illustrations par <a href="https://undraw.co/" target="_blank">UnDraw</a>


## Un lien :
Source pour [mkdocs-material](https://squidfunk.github.io/mkdocs-material/)

## Une image :
![illustration mkdocs-material](https://squidfunk.github.io/mkdocs-material/assets/images/illustration.png)

